python-boltons (25.0.0-1) unstable; urgency=medium

  [ Leandro Cunha ]
  * New upstream version 25.0.0
    + Improve Python 3.13 support
  * d/control:
    - Add package pybuild-plugin-pyproject in build-deps
    - Add package flit in build-deps
    - Bump Standards-Version to 4.7.0

 -- Boyuan Yang <byang@debian.org>  Fri, 07 Feb 2025 13:56:50 -0500

python-boltons (24.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Tentative Python 3.13 support (closes: #1082199).

 -- Colin Watson <cjwatson@debian.org>  Tue, 05 Nov 2024 00:23:19 +0000

python-boltons (24.0.0-1) unstable; urgency=medium

  * New upstream version.
    + Python 3.12 transition: Added Python 3.12 support.
    + Fix [dictutils.OneToOne][dictutils.OneToOne]'s `update()` behavior with
      empty iterables.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Fri, 05 Apr 2024 13:01:55 -0300

python-boltons (23.1.1-1) unstable; urgency=medium

  * New upstream version.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Wed, 21 Feb 2024 23:24:47 -0300

python-boltons (23.0.0-1) unstable; urgency=medium

  * New upstream version.
  * d/control:
    - New uploader and remove DD emeritus Hugo Lefeuvre. (Closes: #1050260)
      NOTE: Thanks Hugo for your contribution with this package!
    - Bump Standards-Version to 4.6.2, no changes needed.
  * d/copyright:
    - Add myself.
    - Update upstream year.
  * Drop d/patches applied by upstream:
    - d/p/address-ecoutils-import-issue-fixes-294.patch reported by Kevin
      Deldycke in https://github.com/mahmoud/boltons/issues/294.
    - d/p/python3.11.patch reported by Stefano Rivera in
      https://github.com/mahmoud/boltons/pull/323.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Thu, 09 Nov 2023 19:52:19 -0300

python-boltons (21.0.0-2) unstable; urgency=medium

  * Team upload.
  * Patch: Python 3.11 support. Closes: #1025023

 -- Stefano Rivera <stefanor@debian.org>  Sun, 25 Dec 2022 10:42:46 -0400

python-boltons (21.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 21.0.0
  * Drop py3.9 compatible patch, add py3.10 compatible patch. Closes: #1002347
  * Update Standards-Version to 4.6.0.
  * Ignore .egg-info files. Builds twice in a row.
  * d/copyright: Update years.
  * Remove recommends and suggests field, no packages listed.
  * Add upstream testsuite as autopkgtest.
  * Remove autopkgtest-pkg-python, redundant when testing with upstream
    testsuite.
  * Update long description, boltons contains more utilities.
  * Remove python3-py as dependency, no longer needed.
  * Add salsa CI

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Tue, 28 Dec 2021 09:01:56 +0000

python-boltons (19.1.0-3) unstable; urgency=medium

  * Team Upload.
  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Nilesh Patra ]
  * Pull upstream fixes for py3.9 (Closes: #973099)
  * Fix with cme
  * Bump Watch file standard to 4

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 16 Dec 2020 02:37:38 +0530

python-boltons (19.1.0-2) unstable; urgency=medium

  * Team upload.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Wed, 31 Jul 2019 14:11:49 +0200

python-boltons (19.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Hugo Lefeuvre ]
  * New upstream release.
  * Bump copyright years.
  * Remove patches/compat-with-python3.7.patch, applied in 19.0.0.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 21 Jul 2019 11:14:16 -0300

python-boltons (18.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 18.0.1
  * Import upstream patch for Python 3.7 (Closes: #912117)
  * Bump Standards-Version to 4.3.0 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 04 Jan 2019 14:44:26 +0100

python-boltons (18.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * debian/control: Set Vcs-* to salsa.debian.org
  * debian/copyright: Use https protocol in Format field
  * debian/control: Remove trailing whitespaces

  [ Hugo Lefeuvre ]
  * New upstream release.
  * Bump compat level to 11.
  * debian/control:
    - Bump Standards-Version to 4.1.3.
    - Depend on debhelper >= 11 instead of >= 10.
    - Declare autopkgtest test suite.
  * Bump copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 25 Mar 2018 11:33:59 +0200

python-boltons (17.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump compat level to 10.
  * debian/control:
    - Bump Standards-Version to 4.0.0.
    - Depend on debhelper >= 10 instead of >= 9.
  * Bump copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 23 Sep 2017 18:07:06 +0200

python-boltons (16.4.1-1) unstable; urgency=medium

  * Initial Debian release (Closes: #837643).

 -- Hugo Lefeuvre <hle@debian.org>  Wed, 14 Sep 2016 13:28:34 +0200
